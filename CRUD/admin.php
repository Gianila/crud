<?php
$conn = new mysqli("localhost","root","","midterms");
if($conn->connect_error){
    die("Connection Failed!".$conn->connect_error);

}
$result = array('error'=>false);
$action = '';

if(isset($_GET['action'])){
    $action = $_GET['action'];   
}


// READ
if($action == 'read'){
    $sql = $conn->query("SELECT * FROM adminids");
    $adminids = array();
    while($row = $sql->fetch_assoc()){
        array_push($adminids, $row);
    }
    $result['adminids'] = $adminids;
}

// CREATE
if($action == 'create'){
    $name = $_POST['name'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = $conn->query("INSERT INTO adminids (name,username,password) VALUES('$name','$username','$password')");
    
    if($sql){
        $result['message'] = "Registered Successfully!";
    }
    else{
        $result['error'] = true;
        $result['message'] = "Failed to Register";
    }
}

// UPDATE
if($action == 'update'){
    $adminid = $_POST['adminid'];
    $name = $_POST['name'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = $conn->query("UPDATE adminids SET name='$name',username='$username',password='$password' WHERE adminid ='$adminid'");
    
    if($sql){
        $result['message'] = "Updated Successfully!";
    }
    else{
        $result['error'] = true;
        $result['message'] = "Failed to Update";
    }
}

// DELETE
if($action == 'delete'){

    $adminid = $_POST['adminid'];
    $sql = $conn->query("DELETE FROM adminids WHERE adminid='$adminid'");
    
    if($sql){
        $result['message'] = "Deleted Successfully!";
    }
    else{
        $result['error'] = true;
        $result['message'] = "Failed to Delete";
    }
}


$conn->close();
echo json_encode($result);
?>