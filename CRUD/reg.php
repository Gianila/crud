<!DOCTYPE html>
<?php
	require_once 'validate.php';
	require 'name.php';
?>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Voters Registration</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/stylew.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">

  <style type="text/css">
    #overlay{
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.6);
}
</style>

  </head>
<body>
    <div id="app">
    <nav class="navbar-expand-md  nav-height shadow-sm">
  <a class="navbar-brand text-light pl-5" href="#">Voters Registration</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" 
        data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" 
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
  </button> 
  <button class="btn float-right pr-5">
    <a href="logout.php" class="btn btn-dark text-light"><span>Logout</span></a>
    <a href="admint.php" class="btn btn-dark"><span>Add Admin</span></a>
  </button>
</nav>


<!-- Table -->

<div class="container pt-4">
<div class="container-xl">
<div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8">
      <h2>Registered</h2><br>
     <div class="" @click="showAddModal=true">
     <a href="#addMember" class="btn btn-dark" data-toggle="modal">
     <i class="fas fa-user-plus"></i> <span>Register</span></a>     
     </div>
   </div>
      </div>
      <hr class="bg-dark">
            <div class="alert alert-danger" v-if="eMsg">
                {{ eMsg }}
            </div>
            <div class="alert alert-success" v-if="sMsg">
                {{ sMsg }}
            </div>
    <table class="table table-striped table-hover table-bordered">
    <thead class="table-head-color">
     <tr class="titles">
      <th> </th>
      <th> </th>
      <th>Name</th>
      <th>Gender</th>
      <th>Age</th>
      <th>Birthdate</th>
      <th>Address</th>
      <th>Contact No.</th>
      </tr>
    </thead>
      <tbody>
     <tr class="text-center bg-light" v-for="voter in members">
     <td><a href="#" class="text-warning" @click="showEditModal=true; selectMember(voter);" 
       data-toggle="modal"><i class="fas fa-edit" data-toggle="tooltip" title="Edit"></i></a>
       <a href="#" class="text-danger" @click="showDeleteModal=true; selectMember(voter);" 
       data-toggle="modal"><i class="fas fa-trash-alt" data-toggle="tooltip" title="Delete"></i></a></td>
       <td>{{ voter.votersid }}</td>
       <td>{{ voter.fname }}</td>
       <td>{{ voter.gender }}</td>
       <td>{{ voter.age }}</td>
       <td>{{ voter.birthdate }}</td>
       <td>{{ voter.address }}</td>
       <td>{{ voter.contact }}</td>
      </tr>
    </tbody>
       </table>
        </div>
    </div>

        <!-- ADD -->
<div id="overlay" v-if="showAddModal">
    <div class="modal-dialog">
     <div class="modal-content">
      <form>
       <div class="modal-header">      
        <h4 class="modal-title">Register</h4>
        <button type="button" class="close" @click="showAddModal=false" 
        data-dismiss="modal" aria-hidden="true">&times;</button>
       </div>
       <div class="modal-body">
           <form action="#" method="post">    
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" placeholder="" 
          v-model="newMember.fname" required>
         </div>
         <div class="form-group">
        <label class="col-form-label req">Gender</label>
      <div class="input-group-prepend"></div>
      <select class="custom-select" id="inputGender" 
      v-model="newMember.gender">
        <option selected>Select Gender</option>
        <option value="Male">Male</option>
        <option value="Female">Female</option>
        <option value="Others">Others</option>
      </select>
        </div>
         <div class="form-group">
          <label>Age</label>
          <input type="text" class="form-control" placeholder="" 
          v-model="newMember.age" required>
         </div>
        <div class="form-group">
         <label>Birthdate</label>
         <input type="text" class="form-control" placeholder="" 
         v-model="newMember.birthdate" required>
        </div>
        <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" placeholder="" 
            v-model="newMember.address" required>
           </div>
           <div class="form-group">
            <label>Contact No.</label>
            <input type="text" class="form-control" placeholder="" 
            v-model="newMember.contact" required>
           </div>
       <div class="modal-footer">
        <input type="button" class="btn btn-danger" @click="showAddModal=false" 
        data-dismiss="modal" value="Cancel">
        <input type="submit" class="btn btn-success" @click="showAddModal=false; addMember()"  
        value="Add">
       </div>
      </form>
     </div>
    </div>
   </div>
</div>
</div>
</div>
<!-- End -->


<!-- EDIT -->
<div id="overlay" v-if="showEditModal">
    <div class="modal-dialog">
     <div class="modal-content">
      <form>
       <div class="modal-header">      
        <h4 class="modal-title">Edit</h4>
        <button type="button" class="close" @click="showEditModal=false" 
        data-dismiss="modal" aria-hidden="true">&times;</button>
       </div>
       <div class="modal-body">
           <form action="#" method="post">    
        
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" v-model="currentMember.fname" required>
         </div>
         <div class="form-group">
        <label class="col-form-label req">Gender:</label>
      <div class="input-group-prepend"></div>
      <select class="custom-select" id="inputGender" 
      v-model="currentMember.gender">
        <option selected>Select Gender</option>
        <option value="Male">Male</option>
        <option value="Female">Female</option>
        <option value="Others">Others</option>
      </select>
        </div>
         <div class="form-group">
          <label>Age</label>
          <input type="text" class="form-control" v-model="currentMember.age" required>
         </div>
        <div class="form-group">
         <label>Birthdate</label>
         <input type="text" class="form-control" v-model="currentMember.birthdate" required>
        </div>
        <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" v-model="currentMember.address" required>
           </div>
           <div class="form-group">
            <label>Contact</label>
            <input type="text" class="form-control" v-model="currentMember.contact" required>
           </div>
         
       </div>
       <div class="modal-footer">
        <input type="button" class="btn btn-danger" 
        @click="showEditModal=false" data-dismiss="modal" value="Cancel">
        <input type="submit" class="btn btn-success" 
        @click="showEditModal=false; updateMember();" value="Save">
       </div>
      </form>
     </div>
    </div>
   </div>
<!-- End -->


<!-- DELETE -->
<div id="overlay" v-if="showDeleteModal">
    <div class="modal-dialog">
     <div class="modal-content">
      <form>
       <div class="modal-header">      
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" @click="showDeleteModal=false" 
        data-dismiss="modal" aria-hidden="true">&times;</button>
       </div>
       <div class="modal-body">     
        <p>Are you sure you want to delete this Record?</p>
        <p class="text-warning"><small>This action cannot be undone.</small></p>
       </div>
       <div class="modal-footer">
        <input type="button" class="btn btn-info" @click="showDeleteModal=false" 
        data-dismiss="modal" value="Cancel">
        <input type="submit" class="btn btn-danger" @click="showDeleteModal=false; deleteMember();" 
        value="Delete">
       </div>
      </form>
     </div>
    </div>
   </div>
   <!-- End -->
   </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.0/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>