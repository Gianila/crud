<?php
$conn = new mysqli("localhost","root","","midterms");
if($conn->connect_error){
    die("Connection Failed!".$conn->connect_error);

}
$result = array('error'=>false);
$action = '';

if(isset($_GET['action'])){
    $action = $_GET['action'];   
}


// READ
if($action == 'read'){
    $sql = $conn->query("SELECT * FROM members");
    $members = array();
    while($row = $sql->fetch_assoc()){
        array_push($members, $row);
    }
    $result['members'] = $members;
}

// CREATE
if($action == 'create'){
    $fname = $_POST['fname'];
    $gender = $_POST['gender'];
    $age = $_POST['age'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];
    $contact = $_POST['contact'];

    $sql = $conn->query("INSERT INTO members (fname,gender,age,birthdate,address,contact) VALUES('$fname','$gender','$age','$birthdate','$address','$contact')");
    
    if($sql){
        $result['message'] = "Registered Successfully!";
    }
    else{
        $result['error'] = true;
        $result['message'] = "Failed to Register";
    }
}

// UPDATE
if($action == 'update'){
    $votersid = $_POST['votersid'];
    $fname = $_POST['fname'];
    $gender = $_POST['gender'];
    $age = $_POST['age'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];
    $contact = $_POST['contact'];

    $sql = $conn->query("UPDATE members SET fname='$fname',gender='$gender',age='$age',birthdate='$birthdate',address='$address',contact='$contact' WHERE votersid='$votersid'");
    
    if($sql){
        $result['message'] = "Updated Successfully!";
    }
    else{
        $result['error'] = true;
        $result['message'] = "Failed to Update";
    }
}

// DELETE
if($action == 'delete'){

    $votersid = $_POST['votersid'];
    $sql = $conn->query("DELETE FROM members WHERE votersid='$votersid'");
    
    if($sql){
        $result['message'] = "Deleted Successfully!";
    }
    else{
        $result['error'] = true;
        $result['message'] = "Failed to Delete";
    }
}


$conn->close();
echo json_encode($result);
?>