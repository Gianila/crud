var app = new Vue({
    el: '#app',
    data: {
        eMsg: "",
        sMsg: "",
        showAddModal: false,
        showEditModal: false,
        showDeleteModal: false,
        members: [],
        newMember: {fname: "",gender: "",age: "",birthdate: "",address: "",contact: ""},
        currentMember: {}
    },
    mounted: function(){
        this.getAllMembers();
    },
    methods:{
        getAllMembers(){
            axios.get("http://localhost/CRUD/process.php?action=read").then(function(response){
    if(response.data.error){
    app.eMsg = response.data.message;
    }
    else{
    app.members = response.data.members;
    }
            });
        },

        addMember(){
            var formData = app.toFormData(app.newMember);
            axios.post("http://localhost/CRUD/process.php?action=create", formData).then(function(response){
        app.newMember = {fname: "",gender: "",age: "",birthdate: "",address: "",contact: ""};
        if(response.data.error){
        app.eMsg = response.data.message;
    }
        else{
        app.sMsg = response.data.message;
        app.getAllMembers();
    }
            });
        },

        updateMember(){
            var formData = app.toFormData(app.currentMember);
            axios.post("http://localhost/CRUD/process.php?action=update", formData).then(function(response){
        app.currentMember = {};
        if(response.data.error){
        app.eMsg = response.data.message;
    }
        else{
        app.sMsg = response.data.message;
        app.getAllMembers();
    }
            });
        },

        deleteMember(){
            var formData = app.toFormData(app.currentMember);
            axios.post("http://localhost/CRUD/process.php?action=delete", formData).then(function(response){
        app.currentMember = {};
        if(response.data.error){
        app.eMsg = response.data.message;
    }
        else{
        app.sMsg = response.data.message;
        app.getAllMembers();
    }
            });
        },

        toFormData(obj){
            var fd = new FormData();
            for(var i in obj){
                fd.append(i,obj[i]);
            }
            return fd;
        },
        selectMember(voter){
            app.currentMember = voter;
        }


    }
});