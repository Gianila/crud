<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Voters Registration</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/stylew.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
<style>
*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
body{
    min-height: 100vh;
    background: #00212E;
    display: flex;
    
}
.container {
    margin: auto;
    width: 500px;
    max-width: 90%;

}
.container form{
    width: 100%;
    height: 100%;
    padding: 20px;
    background: #3281a0;
    border-radius: 4px;
    box-shadow: 0 8px 16px rgba(0,0,0,.3);
}
.container form h1{
    text-align: center;
    margin-bottom:24px;
    color: #222;

}
.container form .form-control{
    width: 100%;
    height: 40px;
    background: white;
    border-radius: 4px;
    border: 1px solid #silver;
    margin: 10px 0 18px 0;
    padding: 0 10px;
}
.container form .btn{
    margin-left: 50%;
    transform: translateX(-50%);
    width: 120px;
    height: 34px;
    border: none;
    outline: none;
    background: #27a327;
    cursor: pointer;
    font-size: 16px;
    text-transform: uppercase;
    color: white;
    border-radius: 4px;
    transition: .3s;
}
.container form .btn:hover{
    opacity: .7;
}
</style>
</head>
<body>
<div id="app">
    <nav class="navbar-expand-md  nav-height shadow-sm">
  <a class="navbar-brand text-light pl-5" href="#">Voters Registration</b></a>
<button class="btn float-right pr-5">
    <a href="admint.php" class="btn btn-dark"><span>Add Admin</span></a>
  </button>

<div class="container pt-5"><br>
<form method="post" action="">
<h1>Login</h1>
<div class="form-group">
<label for="">Username</label>
<input type="text" class="form-control" required>
</div>
<div class="form-group">
<label for="">Password</label>
<input type="Password" class="form-control" required>
</div>
<input type="submit" class="btn" value="login">  
<?php include ('login.php');?>
</form>

</div>

</body>
</html>